# PiNet_SIGIR2019

If you want to use our codes and datasets in your research, please cite:
π-Net: A Parallel Information-sharing Network for Shared-account Cross-domain Sequential Recommendations, SIGIR2019
Muyang Ma, Pengjie Ren, Yujie Lin, Zhumin Chen, Jun Ma, Maarten de Rijke
